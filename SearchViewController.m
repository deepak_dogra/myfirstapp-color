
//
//  SearchViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "SearchViewController.h"
#import "ColorCollectionModel.h"
#import "WebServiceHandler.h"
#import "CustomColorCellTableViewCell.h"
#import "ColorOperations.h"
#import "SavedPaletteListViewController.h"
#import "UserDataCollectionModel.h"
#import "LoginViewController.h"
#import <CoreData/CoreData.h>


@interface SearchViewController () <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIAlertViewDelegate>
/*!
 @brief The SearchViewController class's listOfColors.
 @discussion We will store the response that we will get from the server in this property.It will contain  custom ColorCollectionModel objects.
 */
@property NSMutableArray *listOfColors;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shortListButton;

/*!
 @discussion When user tap on this button then it will show the list of saved Palettes if the user is login otherwise it will redirect the user to login page.
 */
- (IBAction)shortListButtonTapped:(id)sender;

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.shortListButton;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection  = NO;
    
    UISearchBar *searchBar = [[UISearchBar alloc]init];
    searchBar.showsCancelButton = YES;
    searchBar.placeholder = @"Color Name/Code";
    self.navigationItem.titleView = searchBar;
    searchBar.delegate = self;
    
    NSString *appendString = @"colors";
    ColorOperations *colorOperationsObj = [[ColorOperations alloc]init];
    
    //making an api call from here to display the list of all the available colors.
    
    [colorOperationsObj connectWithServer:appendString
                       dataForPostRequest:nil
                            typeOfRequest:3
                    withCompletionHandler:^(NSMutableArray *colorArray)
                    {
                        self.listOfColors = [[NSMutableArray alloc]init];
                        self.listOfColors = colorArray;
                        [self.tableView reloadData];
                    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomColorCellTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:@"Cell"];
}

#pragma mark Show Saved palette Method

- (IBAction)shortListButtonTapped:(id)sender
{
    UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
    
    //if user is logged in only then we will show the list of saved palettes.
    
    if([userDataModelObj.loginStatus isEqual:@"1"])
    {
        SavedPaletteListViewController *savedPaletteListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SavedPaletteListViewController"];
        [self.navigationController pushViewController:(savedPaletteListViewController)
                                             animated:YES];
        
    }
    else //show an alert to user and redirect the user to login page.(using delegate method of UIAlertViewDelegate)
    {
        [[[UIAlertView alloc] initWithTitle:@"Login Required!!"
                                    message:@"Login or Register First"
                                   delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
    }
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listOfColors count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    CustomColorCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    ColorCollectionModel *colorObj= (ColorCollectionModel*) [self.listOfColors objectAtIndex:indexPath.row];
    
    cell.colorFamilyLabel.text = colorObj.colorFamily;
    cell.shadeNameLabel.text   = colorObj.shadeCode;
    
    UIColor *backGroundColor=[UIColor colorWithRed:[colorObj.rgbRedValue floatValue]/255.0f
                                             green:[colorObj.rgbGreenValue floatValue]/255.0f
                                              blue:[colorObj.rgbBlueValue floatValue ]/255.0f
                                             alpha:1.0f];
    
    const CGFloat *componentColors = CGColorGetComponents(backGroundColor.CGColor);
    
    //newColor is the complimentary color of the background color.
    //For every cell its text color should be the complement of its background color.
    
    UIColor *newColor = [[UIColor alloc] initWithRed:(1.0 - componentColors[0])
                                               green:(1.0 - componentColors[1])
                                                blue:(1.0 - componentColors[2])
                                               alpha:componentColors[3]];
    
    cell.backgroundColor = backGroundColor;
    cell.shadeNameLabel.textColor   = newColor;
    cell.colorFamilyLabel.textColor = newColor;
    
    [cell.contentView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.contentView.layer setBorderWidth:2.0f];
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark UISearchBarDelegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    NSString *appendString;
    NSString *letterRegex = @"[A-Za-z ]+";
    NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", letterRegex];
    
    //firstly we need to decide whether the user wants to search based on shade code or for shadename.
    
    if (![namePredicate evaluateWithObject:searchBar.text]) // search for the shadecode
    {
        appendString = [[NSString alloc]initWithFormat:@"color/shade_code=%@",searchBar.text];
    }
    else   // make an api call to search for the shadename
    {
        appendString = [[NSString alloc]initWithFormat:@"color/shade_name=%@",searchBar.text];
    }

    ColorOperations *colorOperationsObj = [[ColorOperations alloc]init];
    
    //making an api call from here for shadename/shadecode.
    
    [colorOperationsObj connectWithServer:appendString
                       dataForPostRequest:nil
                            typeOfRequest:3
                    withCompletionHandler:^(NSMutableArray *colorArray)
    {
        self.listOfColors = [[NSMutableArray alloc]init];
        self.listOfColors = colorArray;
        [self.tableView reloadData];
    }];
}

#pragma mark UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    LoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:(loginViewController)
                                         animated:YES];
}


@end
