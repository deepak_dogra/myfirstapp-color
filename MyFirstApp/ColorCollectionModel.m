//
//  ColorList.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "ColorCollectionModel.h"

@interface ColorCollectionModel () <NSCoding>

@end

@implementation ColorCollectionModel

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.pageNo        = [aDecoder decodeObjectForKey:@"pageNo"];
        self.rgbRedValue   = [aDecoder decodeObjectForKey:@"rgbRedValue"];
        self.rgbGreenValue = [aDecoder decodeObjectForKey:@"rgbGreenValue"];
        self.rgbBlueValue  = [aDecoder decodeObjectForKey:@"rgbBlueValue"];
        self.shadeCode     = [aDecoder decodeObjectForKey:@"shadeCode"];
        self.shadeName     = [aDecoder decodeObjectForKey:@"shadeName"];
        self.shadePosition = [aDecoder decodeObjectForKey:@"shadePosition"];
        self.colorFamily   = [aDecoder decodeObjectForKey:@"colorFamily"];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.pageNo forKey:@"pageNo"];
    [aCoder encodeObject:self.rgbRedValue forKey:@"rgbRedValue"];
    [aCoder encodeObject:self.rgbGreenValue forKey:@"rgbGreenValue"];
    [aCoder encodeObject:self.rgbBlueValue forKey:@"rgbBlueValue"];
    [aCoder encodeObject:self.shadeCode forKey:@"shadeCode"];
    [aCoder encodeObject:self.shadeName forKey:@"shadeName"];
    [aCoder encodeObject:self.shadePosition forKey:@"shadePosition"];
    [aCoder encodeObject:self.colorFamily forKey:@"colorFamily"];
}

@end
