//
//  UserDataCollectionModel.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 14/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "UserDataCollectionModel.h"

@implementation UserDataCollectionModel

static UserDataCollectionModel *userDataModelObj=nil;

+(id) sharedInstance
{
    if(!userDataModelObj)
    {
        userDataModelObj=[[UserDataCollectionModel alloc]init];
    }
    
    return userDataModelObj;
}

-(id) init
{
    if (!userDataModelObj)
    {
        userDataModelObj=[super init];
    }
    return userDataModelObj;
}

@end
