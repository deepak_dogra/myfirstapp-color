//
//  RegisterViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 07/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "RegisterViewController.h"
#import "CityListViewController.h"
#import "OptionsViewController.h"
#import "UserLoginAndRegistration.h"
#import "UserDataCollectionModel.h"

@interface RegisterViewController ()<UITextFieldDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;


- (IBAction)backgroundTap:(id)sender;
- (IBAction)registerButtonTapped:(id)sender;

@end


@implementation RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"image.jpg"]];
    
    [self.nameTextField setDelegate:self];
    [self.emailTextField setDelegate:self];
    [self.mobileNumberTextField setDelegate:self];
    [self.passwordTextField setDelegate:self];
    [self.confirmPasswordTextField setDelegate:self];
    [self.cityTextField setDelegate:self];
}

- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)registerButtonTapped:(id)sender
{
    NSString *errorMessage = [self validateForm];  //check for validation.
    if (errorMessage)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error!!"
                                    message:errorMessage
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
        return;
    }
    else
    {
        NSArray *keys   = @[@"name",@"email",@"password",@"contact_num",@"city"];
        NSArray *values = @[self.nameTextField.text,self.emailTextField.text,self.passwordTextField.text,self.mobileNumberTextField.text,self.cityTextField.text];
        NSDictionary *jsonDictionary = [[NSDictionary alloc]initWithObjects:values
                                                                    forKeys:keys];
        NSData *jsonRequest = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                              options:0
                                                                error:nil];
        
        NSString *appendString;
        appendString = [[NSString alloc]initWithFormat:@"register"];
        
        UserLoginAndRegistration *userObj = [[UserLoginAndRegistration alloc]init];
        
        //making an api call from here for register.
        
        [userObj connectWithServer:appendString
                dataForPostRequest:jsonRequest
                     typeOfRequest:1
             withCompletionHandler:^(NSMutableArray *registerResponse)
         {
             
             NSString *resultStatus  = [registerResponse objectAtIndex:0];
             NSString *resultMessage = [registerResponse objectAtIndex:1];
             
             if([resultStatus isEqual:[NSNumber numberWithInt:0]])
             {
                 [[[UIAlertView alloc] initWithTitle:@"Error!!"
                                             message:resultMessage
                                            delegate:nil
                                   cancelButtonTitle:nil
                                   otherButtonTitles:@"Ok", nil] show];
             }
             else //successful registration
             {
                 NSArray *keys   = @[@"email",@"password"];
                 NSArray *values = @[_emailTextField.text,_passwordTextField.text];
                 NSDictionary *jsonDictionary = [[NSDictionary alloc]initWithObjects:values
                                                                             forKeys:keys];
                 NSData *jsonRequest = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                                       options:0
                                                                         error:nil];
                 NSString *appendString;
                 appendString = [[NSString alloc]initWithFormat:@"login"];
                 
                 //in case of successful registration we are not getting the profileId in response from the server.
                 //so we need to make a login request to set that profileId.
                 
                 [userObj connectWithServer:appendString
                         dataForPostRequest:jsonRequest
                              typeOfRequest:0
                      withCompletionHandler:^(NSMutableArray *registerResponse)
                  {
                      UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
                      userDataModelObj.loginStatus = @"1";
                  }];
                 
                 [[[UIAlertView alloc] initWithTitle:@"Success!!"
                                             message:resultMessage
                                            delegate:self
                                   cancelButtonTitle:nil
                                   otherButtonTitles:@"Ok", nil] show];
             }
         }];
    }
}

#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(self.cityTextField == textField)
    {
        CityListViewController *cityViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CityListViewController"];
    
        //  Setting the block here and it will be called from CityListViewController.
        [cityViewController setGetCityNameBlock:^(NSString * city)
        {
            //we are setting the cityTextField to the city on which user tap in CityListViewController.
            self.cityTextField.text = city;
        }];
        
        [self.navigationController pushViewController:(cityViewController)
                                             animated:YES];
        [self.cityTextField resignFirstResponder];
    }
}

#pragma mark Form Validation Method

- (NSString *)validateForm
{
    NSString *errorMessage;
    
    NSString *regex       = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSString *numregex    = @"[0-9]+";
    NSString *letterregex = @"[A-Za-z ]+";
    
    NSPredicate *emailPredicate  = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numregex];
    NSPredicate *namePredicate   = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", letterregex];
    
    
    if([self.nameTextField.text isEqual:@""]||[self.emailTextField.text isEqual:@""]||[self.mobileNumberTextField.text isEqual:@""]||[self.passwordTextField.text isEqual:@""])
        errorMessage = @"Fill the form properly.";
    
    else if (![namePredicate evaluateWithObject:self.nameTextField.text])
        errorMessage = @"Name Invalid.";
    
    else if(self.nameTextField.text.length<3)
        errorMessage = @"Name must have atleast 3 letters.";
    
    else if (![emailPredicate evaluateWithObject:self.emailTextField.text])
        errorMessage = @"Email Invalid.";
    
    else if (![mobilePredicate evaluateWithObject:self.mobileNumberTextField.text])
        errorMessage = @"Mobile Number invalid.";
    
    else if(self.mobileNumberTextField.text.length!=10)
        errorMessage = @"Mobile Number should be atleast 10 digits.";
    
    else if(self.passwordTextField.text.length<5)
        errorMessage = @"Length of password must be atleast 5.";
    
    else if(![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
        errorMessage = @"Passwords does not match.";
    
    else if([self.cityTextField.text isEqual:@""])
        errorMessage = @"Enter city.";
    
    return errorMessage;
}

#pragma mark UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    OptionsViewController *optionsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"OptionsViewController"];
    [self.navigationController pushViewController:(optionsViewController)
                                         animated:YES];
}

@end
