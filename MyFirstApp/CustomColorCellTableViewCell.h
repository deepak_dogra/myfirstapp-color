//
//  CustomColorCellTableViewCell.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomColorCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *colorFamilyLabel;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UILabel *shadeNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end
