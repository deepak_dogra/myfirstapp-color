//
//  PageContentViewController.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 21/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorCollectionModel.h"

@interface PageContentViewController : UITableViewController

/*!
 @brief The PageContentViewController class's pageIndex.
 @discussion Stores the index of the current displaying page.Its range will be the Number of pages that we want to display.It can't be negative.
 */
@property NSUInteger pageIndex;

/*!
 @brief The PageContentViewController class's getColorFamilyBlock block.
 @discussion This block takes three arguments of (NSString *ColorFamilyName,UIColor *backgroundColor,NSNumber *pageNumber) and its return type is void.It is invoked when we get response from the server.ColorFamilyName is the name of the colorfamily of the colors displaying for the current pageIndex.backgroundColor is the color of the first color of the page.pageNumber is the pageIndex.
 */
@property (copy) void (^getColorFamilyBlock)(NSString *,UIColor *,NSNumber *);

@end
