//
//  UserLoginAndRegistration.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 13/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "UserLoginAndRegistration.h"
#import "UserDataCollectionModel.h"
#import "CityListCollectionModel.h"

@implementation UserLoginAndRegistration

#pragma mark NSURLConnectionDataDelegate Methods.

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *resultStatus,*resultMessage;
    NSError *errorMessage = nil;
    NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData: self.responseData
                                                                     options: NSJSONReadingMutableContainers
                                                                       error: &errorMessage];
    
    NSMutableArray *responseArray = [[NSMutableArray alloc]init];
    
    if(self.requestIdentifier == loginRequest)
    {
        resultStatus  = [resultDictionary valueForKey:@"status"];
        resultMessage = [resultDictionary valueForKey:@"message"];
        
        UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
        
        [responseArray addObject:resultStatus];
        [responseArray addObject:resultMessage];
       
        if([resultStatus isEqual:[NSNumber numberWithInt:1]])  //successful login,set the fields for the current user to maintain the session.
        {
            userDataModelObj.userCity.cityName = [[resultDictionary objectForKey:@"User"]valueForKey:@"city"];
            userDataModelObj.userName          = [[resultDictionary objectForKey:@"User"]valueForKey:@"name"];
            userDataModelObj.userEmail         = [[resultDictionary objectForKey:@"User"]valueForKey:@"email"];
            userDataModelObj.userMobileNumber  = [[resultDictionary objectForKey:@"User"]valueForKey:@"contact_num"];
            userDataModelObj.userPassword      = [[resultDictionary objectForKey:@"User"]valueForKey:@"password"];
            userDataModelObj.profileId         = [[resultDictionary objectForKey:@"User"]valueForKey:@"profileId"];
        }
    }
    
    else if(self.requestIdentifier == registerRequest)
    {
        resultStatus  = [resultDictionary valueForKey:@"success"];
        resultMessage = [resultDictionary valueForKey:@"message"];
        
        [responseArray addObject:resultStatus];
        [responseArray addObject:resultMessage];
    }
    else if((self.requestIdentifier == fetchCityRequest))
    {
        NSArray *tempArray = [resultDictionary objectForKey:@"City"];
        
        for (NSDictionary *dict in tempArray)
        {
            CityListCollectionModel *cityListModelObject = [[CityListCollectionModel alloc]init];
            
            cityListModelObject.cityName  = [dict valueForKey:@"cityName"];
            cityListModelObject.cityId    = [dict valueForKey:@"id"];
            cityListModelObject.stateName = [dict valueForKey:@"stateName"];
            
            [responseArray addObject:cityListModelObject];
        }
    }
    //invoking the block.
    
    if(self.tempStorageBlock)
    {
        self.tempStorageBlock(responseArray);
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                          message:@"Device has no camera"
//                                                         delegate:nil
//                                                cancelButtonTitle:@"OK"
//                                                otherButtonTitles: nil];
//    [myAlertView show];
//    NSLog(@"Failed");
}

@end
