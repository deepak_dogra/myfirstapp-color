//
//  WebServiceHandler.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface WebServiceHandler : NSObject<NSURLConnectionDataDelegate,UIAlertViewDelegate>
/*!
 @brief The WebServiceHandler class's responseData.
 @discussion Stores the response that we get from the server.
 */
@property NSMutableData *responseData;

/*!
 @brief The WebServiceHandler class's tempStorageBlock block.
 @discussion It is a persistent block which takes an NSMutableArray as an argument and it is invoked after when we receive response from the server.
 */
@property (copy) void (^tempStorageBlock)(NSMutableArray *);

/*!
 * @typedef typeOfRequest
 * @brief Tells us about the type of request i.e. whether it is a login or register or some other type of request.
 */
typedef enum typeOfRequest
{
    loginRequest,
    registerRequest,
    fetchCityRequest,
    other
} TypeOfRequest;

@property TypeOfRequest requestIdentifier;

/*!
 @discussion Make connection with the server and store or fetch the data depending on the type of Request(POST or GET).
 @param appendUrlString An NSString that we append to the URL.
 @param dataForPostRequest An NSData that we will post on the server.In case of GET request this field will be nil.
 @param typeOfRequest An enum of type TypeOfRequest to decide the type of request(login or register or fetch city etc).
 @param withCompletionHandler A block which will execute after the successful completion of the method.This block will execute after fetching the response from the server.
 */
-(void) connectWithServer :(NSString *)appendUrlString
        dataForPostRequest:(NSData *)jsonRequest
             typeOfRequest:(TypeOfRequest)typeOfRequest
     withCompletionHandler:(void (^)(NSMutableArray *))callBackBlock ;

@end
