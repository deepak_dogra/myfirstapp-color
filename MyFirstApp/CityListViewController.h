//
//  CityListViewController.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 07/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityListViewController : UITableViewController

/*!
 @brief The CityListViewController class's getCityNameBlock block.
 @discussion This block takes an argument of (NSString *) and its return type is void.It is invoked when user select a city from the list of available cities.
 */
@property (copy) void (^getCityNameBlock)(NSString *);

@end
