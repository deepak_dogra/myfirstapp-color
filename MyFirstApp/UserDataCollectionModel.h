//
//  UserDataCollectionModel.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 14/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityListCollectionModel.h"

@interface UserDataCollectionModel : NSObject

@property NSString *userName;
@property NSString *userEmail;
@property NSString *userPassword;
@property NSString *userMobileNumber;
@property CityListCollectionModel *userCity;
@property NSString *loginStatus;
@property NSString *profileId;

+(id) sharedInstance;
/// UserDataCollectionModel id instanctype

@end
