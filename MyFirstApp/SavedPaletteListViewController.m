//
//  SavedPaletteListViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 25/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "SavedPaletteListViewController.h"
#import "CustomPaletteCellTableViewCell.h"
#import "ColorCollectionModel.h"
#import <CoreData/CoreData.h>


@interface SavedPaletteListViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *deleteButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;

@property (nonatomic, strong) NSMutableArray *savedPaletteArray;

/*!
 @discussion Fetches Data from the core data.
 */
-(void) fetchDataFromCoreData;

@end

@implementation SavedPaletteListViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    [self fetchDataFromCoreData];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomPaletteCellTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:@"paletteCell"];
    
    [self updateButtonsToMatchTableState];
}

#pragma mark Core Data method.

-(void) fetchDataFromCoreData
{
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"SavedListOfPalettes"
                                              inManagedObjectContext:managedObjectContext];
    
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest
                                                                  error:&error];
    self.savedPaletteArray = [[NSMutableArray alloc]init];
    self.savedPaletteArray = [NSMutableArray arrayWithArray:fetchedObjects];
}

#pragma mark Action Methods

- (IBAction)editAction:(id)sender
{
    [self.tableView setEditing:YES animated:YES];
    [self updateButtonsToMatchTableState];
}

- (IBAction)cancelAction:(id)sender
{
    [self.tableView setEditing:NO animated:YES];
    [self updateButtonsToMatchTableState];
}

- (IBAction)deleteAction:(id)sender
{
    NSArray *selectedRows =  [self.tableView indexPathsForSelectedRows];
    
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSMutableIndexSet *indicesOfItemsToDelete = [NSMutableIndexSet new];
    
    for (NSIndexPath *indexPath in selectedRows)
    {
        [managedObjectContext deleteObject:[self.savedPaletteArray objectAtIndex:indexPath.row]];
        
        [indicesOfItemsToDelete addIndex:indexPath.row];
    }
    NSError *error = nil;
    if (![managedObjectContext save:&error])
    {
        NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
    }
    
    [self.savedPaletteArray removeObjectsAtIndexes:(indicesOfItemsToDelete)];
    [self.tableView deleteRowsAtIndexPaths:selectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
    [self fetchDataFromCoreData];
    
    [self updateButtonsToMatchTableState];
    [self updateDeleteButtonTitle];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.savedPaletteArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"paletteCell";
    CustomPaletteCellTableViewCell *cell   = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSManagedObject *managedObject=[self.savedPaletteArray objectAtIndex:indexPath.row];
    
    NSData *baseColorData=[managedObject valueForKey:@"baseColor"];
    NSData *firstColorData=[managedObject valueForKey:@"firstColor"];
    NSData *secondColorData=[managedObject valueForKey:@"secondColor"];

    ColorCollectionModel *baseColorObj  = [[ColorCollectionModel alloc]init];
    ColorCollectionModel *firstColorObj = [[ColorCollectionModel alloc]init];
    ColorCollectionModel *secondColorObj= [[ColorCollectionModel alloc]init];

    baseColorObj   = [NSKeyedUnarchiver unarchiveObjectWithData:baseColorData];
    firstColorObj  = [NSKeyedUnarchiver unarchiveObjectWithData:firstColorData];
    secondColorObj = [NSKeyedUnarchiver unarchiveObjectWithData:secondColorData];
    
    UIColor *firstViewBackGroundColor = [UIColor colorWithRed:[firstColorObj.rgbRedValue floatValue]/255.0f
                                                       green:[firstColorObj.rgbGreenValue floatValue]/255.0f
                                                        blue:[firstColorObj.rgbBlueValue floatValue]/255.0f
                                                       alpha:1.0f];


    UIColor *secondViewBackGroundColor = [UIColor colorWithRed:[baseColorObj.rgbRedValue floatValue]/255.0f
                                                      green:[baseColorObj.rgbGreenValue floatValue]/255.0f
                                                       blue:[baseColorObj.rgbBlueValue floatValue]/255.0f
                                                      alpha:1.0f];
    
    UIColor *thirdViewBackGroundColor = [UIColor colorWithRed:[secondColorObj.rgbRedValue floatValue]/255.0f
                                                       green:[secondColorObj.rgbGreenValue floatValue]/255.0f
                                                        blue:[secondColorObj.rgbBlueValue floatValue]/255.0f
                                                       alpha:1.0f];
    
    cell.label1.backgroundColor = firstViewBackGroundColor;
    cell.label2.backgroundColor = secondViewBackGroundColor;
    cell.label3.backgroundColor = thirdViewBackGroundColor;
    cell.tintColor = [UIColor redColor];
    
    return cell;
    
}

#pragma mark UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [self updateDeleteButtonTitle];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateDeleteButtonTitle];
}

#pragma mark Update Button Methods.

- (void)updateButtonsToMatchTableState
{
    if (self.tableView.editing)
    {
        self.navigationItem.rightBarButtonItem = self.cancelButton;
        [self updateDeleteButtonTitle];
        self.navigationItem.leftBarButtonItem = self.deleteButton;
    }
    else
    {
        self.navigationItem.leftBarButtonItem = self.backButton ;
        if (self.savedPaletteArray.count > 0)
        {
            self.editButton.enabled = YES;
        }
        else
        {
            self.editButton.enabled = NO;
        }
        self.navigationItem.rightBarButtonItem = self.editButton;
    }
}

- (void)updateDeleteButtonTitle
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    BOOL allItemsAreSelected = selectedRows.count == self.savedPaletteArray.count;
    BOOL noItemsAreSelected = selectedRows.count == 0;
    
    if(noItemsAreSelected)
    {
        self.deleteButton.enabled=NO;
        self.deleteButton.title = @"Delete";

    }
    else if (allItemsAreSelected)
    {
        self.deleteButton.enabled=YES;
        self.deleteButton.title = NSLocalizedString(@"Delete All", @"");
    }
    else
    {
        self.deleteButton.enabled=YES;
        NSString *titleFormatString = NSLocalizedString(@"Delete (%d)", @"Title for delete button with placeholder for number");
        self.deleteButton.title = [NSString stringWithFormat:titleFormatString, selectedRows.count];
    }
}

@end



