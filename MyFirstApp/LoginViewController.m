//
//  ViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 03/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "OptionsViewController.h"
#import "UserLoginAndRegistration.h"
#import "UserDataCollectionModel.h"
#import "SavedPaletteListViewController.h"

@interface LoginViewController () <UITextFieldDelegate, NSURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)skipButtonTapped:(id)sender;
- (IBAction)registerButtonTapped:(id)sender;
- (IBAction)backgroundTap:(id)sender;

@end

@implementation LoginViewController

#pragma mark View Lifecycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //if we have been redirected here from some other ViewController ,i.e. it is not the intial ViewController.
    //then also we dont want to show the back button
    if([self.navigationController.viewControllers count] != 1)
    {
        self.navigationItem.hidesBackButton = YES;
    }
    self.emailTextField.delegate    = self;
    self.passwordTextField.delegate = self;
    self.view.backgroundColor       = [UIColor colorWithPatternImage:[UIImage imageNamed:@"image.jpg"]];
}

#pragma mark Login and Registration method

- (IBAction)loginButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    
    NSArray *keys   =   @[@"email",@"password"];
    NSArray *values =   @[self.emailTextField.text,self.passwordTextField.text];
    NSDictionary *jsonDictionary = [[NSDictionary alloc]initWithObjects:values
                                                                forKeys:keys];
    
    NSData *jsonRequest = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                          options:0
                                                            error:nil];
    
    NSString *appendString;
    appendString=[[NSString alloc]initWithFormat:@"login"];
    UserLoginAndRegistration *userObj=[[UserLoginAndRegistration alloc]init];
    
    //making an api call from here for login.
    
    [userObj connectWithServer:appendString
            dataForPostRequest:jsonRequest
                 typeOfRequest:0
         withCompletionHandler:^(NSMutableArray *loginResponse)
        {
             
             NSString *resultStatus  = [loginResponse objectAtIndex:0];
             NSString *resultMessage = [loginResponse objectAtIndex:1];
             
             if([resultStatus isEqual:[NSNumber numberWithInt:0]]) //Invalid Username or password
             {
                 [[[UIAlertView alloc] initWithTitle:@"Error!!"
                                             message:resultMessage
                                            delegate:nil
                                   cancelButtonTitle:nil
                                   otherButtonTitles:@"Ok", nil] show];
                 return;
             }
             else
             {
                 UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
                 userDataModelObj.loginStatus = @"1";
                
                 if([self.navigationController.viewControllers count] == 1)
                 {
                     OptionsViewController *optionsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OptionsViewController"];
                     [self.navigationController pushViewController:(optionsViewController)
                                                          animated:YES];
                 }
                 //we have been redirected to LoginViewController and we need to login first
                 //only then we can see the list of saved palettes.
                 else
                 {
                     SavedPaletteListViewController *savedPaletteListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SavedPaletteListViewController"];
                     [self.navigationController pushViewController:(savedPaletteListViewController)
                                                          animated:YES];
                 }
                
             }
        }];
    
}

//If user don't want to login then he will press skip button.

- (IBAction)skipButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    OptionsViewController *optionsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OptionsViewController"];
    [self.navigationController pushViewController:(optionsViewController)
                                         animated:YES];
}

- (IBAction)registerButtonTapped:(id)sender
{
    [self.view endEditing:YES];

    RegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:(registerViewController)
                                         animated:YES];
}

- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
