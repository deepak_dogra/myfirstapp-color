//
//  ColorList.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorCollectionModel : NSObject

@property NSNumber *rgbBlueValue;
@property NSNumber *rgbGreenValue;
@property NSNumber *rgbRedValue;
@property NSNumber *pageNo;
@property NSString *colorFamily;
@property NSString *shadeCode;
@property NSString *shadeName;
@property NSString *shadePosition;

@end
