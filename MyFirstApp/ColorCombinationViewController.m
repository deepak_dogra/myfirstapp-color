//
//  ColorCombinationViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 22/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "ColorCombinationViewController.h"
#import "PageContentViewController.h"
#import "ColorCollectionModel.h"
#import "ColorOperations.h"
#import "UserDataCollectionModel.h"
#import "SavedPaletteListViewController.h"
#import <CoreData/CoreData.h>

@interface ColorCombinationViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ColorCollectionModel *analogousFirstColorObject;
    ColorCollectionModel *analogousSecondColorObject;
    ColorCollectionModel *monochromaticFirstColorObject;
    ColorCollectionModel *monochromaticSecondColorObject;
    ColorCollectionModel *complimentaryFirstColorObject;
    ColorCollectionModel *complimentarySecondColorObject;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *shortListButton;

//Three outlets for displaying three analogous colors for the selected color(i.e. for colorPaletteObj).

@property (weak, nonatomic) IBOutlet UIView *analogousFirstView;
@property (weak, nonatomic) IBOutlet UIView *analogousSecondView;
@property (weak, nonatomic) IBOutlet UIView *analogousThirdView;

//Three outlets for displaying three monochromatic colors for the selected color(i.e. for colorPaletteObj).

@property (weak, nonatomic) IBOutlet UIView *monochromaticFirstView;
@property (weak, nonatomic) IBOutlet UIView *monochromaticSecondView;
@property (weak, nonatomic) IBOutlet UIView *monochromaticThirdView;

//Three outlets for displaying three complimentary colors for the selected color(i.e. for colorPaletteObj).

@property (weak, nonatomic) IBOutlet UIView *complimentaryFirstView;
@property (weak, nonatomic) IBOutlet UIView *complimentarySecondView;
@property (weak, nonatomic) IBOutlet UIView *complimentaryThirdView;

//Three outlets for displaying the colorName for the selected color in analogous ,monochromatic and complimentary view.

@property (weak, nonatomic) IBOutlet UILabel *firstShadeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondShadeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdShadeNameLabel;

/*!
 @discussion When user tap on this button then it will show the list of saved Palettes if the user is login otherwise it will redirect the user to login page.
 */
- (IBAction)shortListButtonTapped:(id)sender;

/*!
 @discussion When user tap on this button it stores the analogous colors in the core data.
 */
- (IBAction)firstSaveMeButtonTapped:(id)sender;

/*!
 @discussion When user tap on this button it stores the monochromatic colors in the core data.
 */
- (IBAction)secondSaveMeButtonTapped:(id)sender;

/*!
 @discussion When user tap on this button it stores the complimentary colors in the core data.
 */
- (IBAction)thirdSaveMeButtonTapped:(id)sender;

/*!
 @discussion We will find the corresponding analogous,monochromatic and complimentary colors using this method.
 @param fetchedObjects An (NSArray *) of (ColorCollectionModel *) which we fetched from the core data.
 */
-(void)findMatchingColorsForGivenPalette:(NSArray *) fetchedObjects;

/*!
 @discussion Stores the colors in the core data.
 */
-(void)storeColorObjectsInCoreData;
-(void)adjustingViews;

@end

@implementation ColorCombinationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.shortListButton;

    self.tableView.allowsSelection  = NO;
    self.tableView.scrollEnabled    = NO;
    
    analogousFirstColorObject       = [[ColorCollectionModel alloc]init];
    analogousSecondColorObject      = [[ColorCollectionModel alloc]init];
    monochromaticFirstColorObject   = [[ColorCollectionModel alloc]init];
    monochromaticSecondColorObject  = [[ColorCollectionModel alloc]init];
    complimentaryFirstColorObject   = [[ColorCollectionModel alloc]init];
    complimentarySecondColorObject  = [[ColorCollectionModel alloc]init];

    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"ListOfColors"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest
                                                                  error:&error];
    if([fetchedObjects count] == 0) //core data is empty.
    {
        [self storeColorObjectsInCoreData];
    }
    
    else
    {
        [self findMatchingColorsForGivenPalette:(fetchedObjects)];
        [self adjustingViews];
    }
}

#pragma mark Core data methods (Store and Fetch)

-(void)storeColorObjectsInCoreData
{
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSString *appendString;
    appendString = [[NSString alloc]initWithFormat:@"colors"];
    ColorOperations *colorOperationsObj = [[ColorOperations alloc]init];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    //making an api call for colors.
    
    [colorOperationsObj connectWithServer:appendString
                       dataForPostRequest:nil
                            typeOfRequest:3
                    withCompletionHandler:^(NSMutableArray *colorDict)
     {
         for (ColorCollectionModel *dict in colorDict) //storing objects in the core data.
         {
             
             NSManagedObject *colorListObj = [NSEntityDescription insertNewObjectForEntityForName:@"ListOfColors"
                                                                           inManagedObjectContext:managedObjectContext];
             
             [colorListObj setValue: dict.rgbBlueValue forKey:@"rgbBlueValue"];
             [colorListObj setValue: dict.rgbGreenValue forKey:@"rgbGreenValue"];
             [colorListObj setValue: dict.rgbRedValue forKey:@"rgbRedValue"];
             [colorListObj setValue: dict.pageNo forKey:@"pageNo"];
             [colorListObj setValue: dict.colorFamily forKey:@"colorFamily"];
             [colorListObj setValue: dict.shadePosition forKey:@"shadePosition"];
             [colorListObj setValue: dict.shadeName forKey:@"shadeName"];
             [colorListObj setValue: dict.shadeCode forKey:@"shadeCode"];
             
             NSError *error;
             if (![managedObjectContext save:&error])
             {
                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
             }
         }
     }];
}


-(void)findMatchingColorsForGivenPalette:(NSArray *)fetchedObjects
{
    NSInteger rValueDiffForMinColor = 0,gValueDiffForMinColor = 0,bValueDiffForMinColor = 0;
    NSInteger rValueDiffForMaxColor = 0,gValueDiffForMaxColor = 0,bValueDiffForMaxColor = 0;
    
    NSInteger requiredAnalogousRedValueForMinColor,requiredAnalogousGreenValueForMinColor,requiredAnalogousBlueValueForMinColor;
    NSInteger requiredAnalogousRedValueForMaxColor,requiredAnalogousGreenValueForMaxColor,requiredAnalogousBlueValueForMaxColor;
    
    NSInteger requiredMonochromaticRedValueForMinColor,requiredMonochromaticGreenValueForMinColor,requiredMonochromaticBlueValueForMinColor;
    NSInteger requiredMonochromaticRedValueForMaxColor,requiredMonochromaticGreenValueForMaxColor,requiredMonochromaticBlueValueForMaxColor;
    
    NSInteger requiredComplimentaryRedValueForMinColor,requiredComplimentaryGreenValueForMinColor,requiredComplimentaryBlueValueForMinColor;
    NSInteger requiredComplimentaryRedValueForMaxColor,requiredComplimentaryGreenValueForMaxColor,requiredComplimentaryBlueValueForMaxColor;
    
    NSInteger minDiffForAnalogousMinColor=255*3;
    NSInteger minDiffForAnalogousMaxColor=255*3;
    NSInteger minDiffForMonochromaticMinColor=255*3;
    NSInteger minDiffForMonochromaticMaxColor=255*3;
    NSInteger minDiffForComplimentaryMinColor=255*3;
    NSInteger minDiffForComplimentaryMaxColor=255*3;
    NSInteger diff;
    
    
    //for analogous
    //min color means color having rgb values less than the rgb values of given palette.
    //max color means color having rgb values greater than the rgb values of given palette.

    requiredAnalogousRedValueForMinColor    = [self.colorPaletteObj.rgbRedValue integerValue];
    requiredAnalogousRedValueForMinColor   -= (0.1)*requiredAnalogousRedValueForMinColor;
    requiredAnalogousGreenValueForMinColor  = [self.colorPaletteObj.rgbGreenValue integerValue];
    requiredAnalogousGreenValueForMinColor -= (0.1)*requiredAnalogousGreenValueForMinColor;
    requiredAnalogousBlueValueForMinColor   = [self.colorPaletteObj.rgbBlueValue integerValue];
    requiredAnalogousBlueValueForMinColor  -= (0.1)*requiredAnalogousBlueValueForMinColor;
    
    requiredAnalogousRedValueForMaxColor  = [self.colorPaletteObj.rgbRedValue integerValue];
    requiredAnalogousRedValueForMaxColor += (0.1)*requiredAnalogousRedValueForMaxColor;
    if(requiredAnalogousRedValueForMaxColor > 255)
    {
        requiredAnalogousRedValueForMaxColor = 255;
    }
    requiredAnalogousGreenValueForMaxColor  = [self.colorPaletteObj.rgbGreenValue integerValue];
    requiredAnalogousGreenValueForMaxColor += (0.1)*requiredAnalogousGreenValueForMaxColor;
    if(requiredAnalogousGreenValueForMaxColor > 255)
    {
        requiredAnalogousGreenValueForMaxColor = 255;
    }
    requiredAnalogousBlueValueForMaxColor  = [self.colorPaletteObj.rgbBlueValue integerValue];
    requiredAnalogousBlueValueForMaxColor += (0.1)*requiredAnalogousBlueValueForMaxColor;
    if(requiredAnalogousBlueValueForMaxColor > 255)
    {
        requiredAnalogousBlueValueForMaxColor = 255;
    }
    
    //for monochromatic
    //min color means color having rgb values less than the rgb values of given palette.
    //max color means color having rgb values greater than the rgb values of given palette.
    
    requiredMonochromaticRedValueForMinColor = ([self.colorPaletteObj.rgbRedValue integerValue]+[self.colorPaletteObj.rgbGreenValue integerValue]+[self.colorPaletteObj.rgbBlueValue integerValue])/3;
    requiredMonochromaticRedValueForMinColor  -= (0.1)*requiredMonochromaticRedValueForMinColor;
    requiredMonochromaticGreenValueForMinColor = requiredMonochromaticRedValueForMinColor;
    requiredMonochromaticBlueValueForMinColor  = requiredMonochromaticRedValueForMinColor;
    
    
    requiredMonochromaticRedValueForMaxColor = ([self.colorPaletteObj.rgbRedValue integerValue]+[self.colorPaletteObj.rgbGreenValue integerValue]+[self.colorPaletteObj.rgbBlueValue integerValue])/3;
    requiredMonochromaticRedValueForMaxColor += (0.1)*requiredMonochromaticRedValueForMaxColor;
    if(requiredMonochromaticRedValueForMaxColor > 255)
    {
        requiredMonochromaticRedValueForMaxColor = 255;
    }
    
    requiredMonochromaticGreenValueForMaxColor = requiredMonochromaticRedValueForMaxColor;
    requiredMonochromaticBlueValueForMaxColor  = requiredMonochromaticRedValueForMaxColor;
    
    
    //for complimentary
    //min color means color having rgb values less than the rgb values of given palette.
    //max color means color having rgb values greater than the rgb values of given palette.
    
    requiredComplimentaryRedValueForMinColor    = 255-[self.colorPaletteObj.rgbRedValue integerValue];
    requiredComplimentaryRedValueForMinColor   -= (0.1)*requiredComplimentaryRedValueForMinColor;
    requiredComplimentaryGreenValueForMinColor  = 255-[self.colorPaletteObj.rgbGreenValue integerValue];
    requiredComplimentaryGreenValueForMinColor -= (0.1)*requiredComplimentaryGreenValueForMinColor;
    requiredComplimentaryBlueValueForMinColor   = 255-[self.colorPaletteObj.rgbBlueValue integerValue];
    requiredComplimentaryBlueValueForMinColor  -= (0.1)*requiredComplimentaryBlueValueForMinColor;
    
    requiredComplimentaryRedValueForMaxColor  = 255-[self.colorPaletteObj.rgbRedValue integerValue];
    requiredComplimentaryRedValueForMaxColor += (0.1)*requiredComplimentaryRedValueForMaxColor;
    if(requiredComplimentaryRedValueForMaxColor > 255)
    {
        requiredComplimentaryRedValueForMaxColor = 255;
    }
    requiredComplimentaryGreenValueForMaxColor  = 255-[self.colorPaletteObj.rgbGreenValue integerValue];
    requiredComplimentaryGreenValueForMaxColor += (0.1)*requiredComplimentaryGreenValueForMaxColor;
    if(requiredComplimentaryGreenValueForMaxColor > 255)
    {
        requiredComplimentaryGreenValueForMaxColor = 255;
    }
    requiredComplimentaryBlueValueForMaxColor  = 255-[self.colorPaletteObj.rgbBlueValue integerValue];
    requiredComplimentaryBlueValueForMaxColor += (0.1)*requiredComplimentaryBlueValueForMaxColor;
    if(requiredComplimentaryBlueValueForMaxColor > 255)
    {
        requiredComplimentaryBlueValueForMaxColor = 255;
    }
    
    for (NSManagedObjectContext *context in fetchedObjects) //search for the matching object in the core data.
    {
        //for analogous..
        
        rValueDiffForMinColor = (labs)(requiredAnalogousRedValueForMinColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMinColor = (labs)(requiredAnalogousGreenValueForMinColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMinColor = (labs)(requiredAnalogousBlueValueForMinColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        rValueDiffForMaxColor = (labs)(requiredAnalogousRedValueForMaxColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMaxColor = (labs)(requiredAnalogousGreenValueForMaxColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMaxColor = (labs)(requiredAnalogousBlueValueForMaxColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        diff = rValueDiffForMinColor+gValueDiffForMinColor+bValueDiffForMinColor;
        
        if(diff < minDiffForAnalogousMinColor && diff != 0)
        {
            analogousFirstColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            analogousFirstColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            analogousFirstColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            analogousFirstColorObject.pageNo        = [context valueForKey:@"pageNo"];
            analogousFirstColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            analogousFirstColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            analogousFirstColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            analogousFirstColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForAnalogousMinColor = diff;
        }
        
        diff = rValueDiffForMaxColor + gValueDiffForMaxColor + bValueDiffForMaxColor;
        
        if(diff < minDiffForAnalogousMaxColor && diff != 0)
        {
            analogousSecondColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            analogousSecondColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            analogousSecondColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            analogousSecondColorObject.pageNo        = [context valueForKey:@"pageNo"];
            analogousSecondColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            analogousSecondColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            analogousSecondColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            analogousSecondColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForAnalogousMaxColor = diff;
        }
        
        rValueDiffForMinColor = (labs)(requiredMonochromaticRedValueForMinColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMinColor = (labs)(requiredMonochromaticGreenValueForMinColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMinColor = (labs)(requiredMonochromaticBlueValueForMinColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        rValueDiffForMaxColor = (labs)(requiredMonochromaticRedValueForMaxColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMaxColor = (labs)(requiredMonochromaticGreenValueForMaxColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMaxColor = (labs)(requiredMonochromaticBlueValueForMaxColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        diff = rValueDiffForMinColor + gValueDiffForMinColor + bValueDiffForMinColor;
        
        if(diff < minDiffForMonochromaticMinColor && diff != 0)
        {
            monochromaticFirstColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            monochromaticFirstColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            monochromaticFirstColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            monochromaticFirstColorObject.pageNo        = [context valueForKey:@"pageNo"];
            monochromaticFirstColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            monochromaticFirstColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            monochromaticFirstColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            monochromaticFirstColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForMonochromaticMinColor = diff;
        }
        
        diff = rValueDiffForMaxColor + gValueDiffForMaxColor + bValueDiffForMaxColor;
        
        if(diff < minDiffForMonochromaticMaxColor && diff != 0)
        {
            monochromaticSecondColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            monochromaticSecondColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            monochromaticSecondColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            monochromaticSecondColorObject.pageNo        = [context valueForKey:@"pageNo"];
            monochromaticSecondColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            monochromaticSecondColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            monochromaticSecondColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            monochromaticSecondColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForMonochromaticMaxColor = diff;
        }
        
        //for complimentary
        
        rValueDiffForMinColor = (labs)(requiredComplimentaryRedValueForMinColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMinColor = (labs)(requiredComplimentaryGreenValueForMinColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMinColor = (labs)(requiredComplimentaryBlueValueForMinColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        rValueDiffForMaxColor = (labs)(requiredComplimentaryRedValueForMaxColor-[[context valueForKey:@"rgbRedValue"] integerValue]);
        gValueDiffForMaxColor = (labs)(requiredComplimentaryGreenValueForMaxColor-[[context valueForKey:@"rgbGreenValue"] integerValue]);
        bValueDiffForMaxColor = (labs)(requiredComplimentaryBlueValueForMaxColor-[[context valueForKey:@"rgbBlueValue"] integerValue]);
        
        diff = rValueDiffForMinColor + gValueDiffForMinColor + bValueDiffForMinColor;
        
        if(diff < minDiffForComplimentaryMinColor && diff != 0)
        {
            complimentaryFirstColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            complimentaryFirstColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            complimentaryFirstColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            complimentaryFirstColorObject.pageNo        = [context valueForKey:@"pageNo"];
            complimentaryFirstColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            complimentaryFirstColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            complimentaryFirstColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            complimentaryFirstColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForComplimentaryMinColor = diff;
        }
        
        diff = rValueDiffForMaxColor + gValueDiffForMaxColor + bValueDiffForMaxColor;
        
        if(diff < minDiffForComplimentaryMaxColor && diff != 0)
        {
            complimentarySecondColorObject.rgbBlueValue  = [context valueForKey:@"rgbBlueValue"];
            complimentarySecondColorObject.rgbGreenValue = [context valueForKey:@"rgbGreenValue"];
            complimentarySecondColorObject.rgbRedValue   = [context valueForKey:@"rgbRedValue"];
            complimentarySecondColorObject.pageNo        = [context valueForKey:@"pageNo"];
            complimentarySecondColorObject.colorFamily   = [context valueForKey:@"colorFamily"];
            complimentarySecondColorObject.shadePosition = [context valueForKey:@"shadePosition"];
            complimentarySecondColorObject.shadeCode     = [context valueForKey:@"shadeCode"];
            complimentarySecondColorObject.shadeName     = [context valueForKey:@"shadeName"];
            
            minDiffForComplimentaryMaxColor = diff;
        }
    }
}


-(void)adjustingViews
{
    self.analogousFirstView.backgroundColor = [UIColor colorWithRed:[analogousFirstColorObject.rgbRedValue floatValue]/255.0f
                                                              green:[analogousFirstColorObject.rgbGreenValue floatValue]/255.0f
                                                               blue:[analogousFirstColorObject.rgbBlueValue floatValue ]/255.0f
                                                              alpha:1.0f];
    
    self.analogousSecondView.backgroundColor = [UIColor colorWithRed:[self.colorPaletteObj.rgbRedValue floatValue]/255.0f
                                                               green:[self.colorPaletteObj.rgbGreenValue floatValue]/255.0f
                                                                blue:[self.colorPaletteObj.rgbBlueValue floatValue ]/255.0f
                                                               alpha:1.0f];
    
    self.analogousThirdView.backgroundColor = [UIColor colorWithRed:[analogousSecondColorObject.rgbRedValue floatValue]/255.0f
                                                              green:[analogousSecondColorObject.rgbGreenValue floatValue]/255.0f
                                                               blue:[analogousSecondColorObject.rgbBlueValue floatValue ]/255.0f
                                                              alpha:1.0f];
    
    self.monochromaticFirstView.backgroundColor = [UIColor colorWithRed:[monochromaticFirstColorObject.rgbRedValue floatValue]/255.0f
                                                                  green:[monochromaticFirstColorObject.rgbGreenValue floatValue]/255.0f
                                                                   blue:[monochromaticFirstColorObject.rgbBlueValue floatValue ]/255.0f
                                                                  alpha:1.0f];
    
    self.monochromaticSecondView.backgroundColor = [UIColor colorWithRed:[self.colorPaletteObj.rgbRedValue floatValue]/255.0f
                                                                   green:[self.colorPaletteObj.rgbGreenValue floatValue]/255.0f
                                                                    blue:[self.colorPaletteObj.rgbBlueValue floatValue ]/255.0f
                                                                   alpha:1.0f];
    
    self.monochromaticThirdView.backgroundColor = [UIColor colorWithRed:[monochromaticSecondColorObject.rgbRedValue floatValue]/255.0f
                                                                  green:[monochromaticSecondColorObject.rgbGreenValue floatValue]/255.0f
                                                                   blue:[monochromaticSecondColorObject.rgbBlueValue floatValue ]/255.0f
                                                                  alpha:1.0f];
    
    self.complimentaryFirstView.backgroundColor = [UIColor colorWithRed:[complimentaryFirstColorObject.rgbRedValue floatValue]/255.0f
                                                                  green:[complimentaryFirstColorObject.rgbGreenValue floatValue]/255.0f
                                                                   blue:[complimentaryFirstColorObject.rgbBlueValue floatValue ]/255.0f
                                                                  alpha:1.0f];
    
    self.complimentarySecondView.backgroundColor = [UIColor colorWithRed:[self.colorPaletteObj.rgbRedValue floatValue]/255.0f
                                                                   green:[self.colorPaletteObj.rgbGreenValue floatValue]/255.0f
                                                                    blue:[self.colorPaletteObj.rgbBlueValue floatValue ]/255.0f
                                                                   alpha:1.0f];
    
    self.complimentaryThirdView.backgroundColor = [UIColor colorWithRed:[complimentarySecondColorObject.rgbRedValue floatValue]/255.0f
                                                                  green:[complimentarySecondColorObject.rgbGreenValue floatValue]/255.0f
                                                                   blue:[complimentarySecondColorObject.rgbBlueValue floatValue ]/255.0f
                                                                  alpha:1.0f];
    
    self.firstShadeNameLabel.text  = self.colorPaletteObj.shadeName;
    self.secondShadeNameLabel.text = self.colorPaletteObj.shadeName;
    self.thirdShadeNameLabel.text  = self.colorPaletteObj.shadeName;
    
}

#pragma mark UITableViewDelegate Method.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.row%2==0)
        return 15;
    else
        return self.tableView.frame.size.height/4;
}

#pragma mark Show Saved palette Method.

- (IBAction)shortListButtonTapped:(id)sender
{
//    UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
    
//    if([userDataModelObj.loginStatus isEqual:@"1"])
    {
        SavedPaletteListViewController *savedPaletteListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SavedPaletteListViewController"];
        [self.navigationController pushViewController:(savedPaletteListViewController)
                                             animated:YES];
        
    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Login Required!!"
//                                    message:@"Login or Register First"
//                                   delegate:self
//                          cancelButtonTitle:nil
//                          otherButtonTitles:@"Ok", nil] show];
//    }

}

#pragma mark Saved palettes Methods

- (IBAction)firstSaveMeButtonTapped:(id)sender
{
    UIButton *button = (UIButton *) sender;
    button.enabled = NO;
    [button setTitle:@"Saved" forState:UIControlStateDisabled];
    button.backgroundColor = [UIColor blackColor];

    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];

    NSData *baseColorDataObject   = [NSKeyedArchiver archivedDataWithRootObject:self.colorPaletteObj];
    NSData *firstColorDataObject  = [NSKeyedArchiver archivedDataWithRootObject:analogousFirstColorObject];
    NSData *secondColorDataObject = [NSKeyedArchiver archivedDataWithRootObject:analogousSecondColorObject];
    
    NSError *error;
    NSManagedObject *colorListObj = [NSEntityDescription insertNewObjectForEntityForName:@"SavedListOfPalettes"
                                                                      inManagedObjectContext:managedObjectContext];
        
    [colorListObj setValue: baseColorDataObject forKey:@"baseColor"];
    [colorListObj setValue: firstColorDataObject forKey:@"firstColor"];
    [colorListObj setValue: secondColorDataObject forKey:@"secondColor"];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Success!!"
                                    message:@"Palette Stored Successfully"
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
        
    }
}

- (IBAction)secondSaveMeButtonTapped:(id)sender
{
    UIButton *button = (UIButton *) sender;
    button.enabled = NO;
    [button setTitle:@"Saved" forState:UIControlStateDisabled];
    button.backgroundColor =  [UIColor blackColor];
    
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSData *baseColorDataObject   = [NSKeyedArchiver archivedDataWithRootObject:self.colorPaletteObj];
    NSData *firstColorDataObject  = [NSKeyedArchiver archivedDataWithRootObject:monochromaticFirstColorObject];
    NSData *secondColorDataObject = [NSKeyedArchiver archivedDataWithRootObject:monochromaticSecondColorObject];
    
    NSError *error;
    NSManagedObject *colorListObj = [NSEntityDescription insertNewObjectForEntityForName:@"SavedListOfPalettes"
                                                                  inManagedObjectContext:managedObjectContext];
    
    [colorListObj setValue: baseColorDataObject forKey:@"baseColor"];
    [colorListObj setValue: firstColorDataObject forKey:@"firstColor"];
    [colorListObj setValue: secondColorDataObject forKey:@"secondColor"];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Success!!"
                                    message:@"Palette Stored Successfully"
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
        
    }
}

- (IBAction)thirdSaveMeButtonTapped:(id)sender
{
    UIButton *button = (UIButton *) sender;
    button.enabled = NO;
    [button setTitle:@"Saved" forState:UIControlStateDisabled];
    button.backgroundColor = [UIColor blackColor];
    
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSData *baseColorDataObject   = [NSKeyedArchiver archivedDataWithRootObject:self.colorPaletteObj];
    NSData *firstColorDataObject  = [NSKeyedArchiver archivedDataWithRootObject:complimentaryFirstColorObject];
    NSData *secondColorDataObject = [NSKeyedArchiver archivedDataWithRootObject:complimentarySecondColorObject];
    
    NSError *error;
    NSManagedObject *colorListObj = [NSEntityDescription insertNewObjectForEntityForName:@"SavedListOfPalettes"
                                                                  inManagedObjectContext:managedObjectContext];
    
    [colorListObj setValue: baseColorDataObject forKey:@"baseColor"];
    [colorListObj setValue: firstColorDataObject forKey:@"firstColor"];
    [colorListObj setValue: secondColorDataObject forKey:@"secondColor"];
    
    if (![managedObjectContext save:&error])
    {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Success!!"
                                    message:@"Palette Stored Successfully"
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
        
    }}
@end
