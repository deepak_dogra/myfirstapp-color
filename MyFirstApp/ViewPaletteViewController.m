//
//  ViewPaletteViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 17/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "ViewPaletteViewController.h"
#import "PageContentViewController.h"
#import "UserDataCollectionModel.h"
#import "LoginViewController.h"
#import "SavedPaletteListViewController.h"

@interface ViewPaletteViewController ()<UIPageViewControllerDataSource,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *shortListButton;
/*!
 @brief The ViewPaletteViewController class's containerView.
 @discussion It will View the contents of pageContentViewController.
 */
@property __weak IBOutlet UIView *containerView;

/*!
 @brief The ViewPaletteViewController class's colorFamilyLabel.
 @discussion Displays the colorFamily of the colors which are displayed in the containerView.
 */
@property (weak, nonatomic) IBOutlet UILabel *colorFamilyLabel;

/*!
 @brief The ViewPaletteViewController class's pageViewController.
 @discussion It is a containerController's object which contains all the pageContentViewControllers.
 */
@property (strong, nonatomic) UIPageViewController *pageViewController;

/*!
 @discussion When user tap on this button then it will show the list of saved Palettes if the user is login otherwise it will redirect the user to login page.
 */
- (IBAction)shortListButtonTapped:(id)sender;
/*!
 @discussion It tells us about which viewController we are going to display at a particular index.
 @param index An NSUInteger tells the index of the Page that we are going to display.
 @warning It can't be nagative.
 @return  A pageContentViewController that we display for the given index.
 */

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index;

@end

@implementation ViewPaletteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.shortListButton;
    self.pageViewController.dataSource = self;
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0]; //initialize the pageViewController
    NSArray *viewControllers = @[startingViewController];
    
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embeddedPageViewControllerSegue"])
    {
        self.pageViewController = segue.destinationViewController;
    }
}


#pragma mark Show Saved palette Method

- (IBAction)shortListButtonTapped:(id)sender
{
    UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
    
    //if user is logged in only then we will show the list of saved palettes.
    
    if([userDataModelObj.loginStatus isEqual:@"1"])
    {
        SavedPaletteListViewController *savedPaletteListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SavedPaletteListViewController"];
        [self.navigationController pushViewController:(savedPaletteListViewController)
                                             animated:YES];
        
    }
    else //show an alert to user and redirect the user to login page.(using delegate method of UIAlertViewDelegate)
    {
        [[[UIAlertView alloc] initWithTitle:@"Login Required!!"
                                    message:@"Login or Register First"
                                   delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
    }
}

#pragma mark Show PageContentViewController Method

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index >= 240)
    {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    
    pageContentViewController.pageIndex = index;
    
    //  Setting the block here and it will be called from PageContentViewController.
    
    [pageContentViewController setGetColorFamilyBlock:^(NSString * colorFamily,UIColor * color,NSNumber *pageNumber)
     {
         self.colorFamilyLabel.text=[NSString stringWithFormat:@"Color Family :%@  ", colorFamily];
         self.colorFamilyLabel.backgroundColor=color;
     }];
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound)
    {
        return nil;
    }
    index++;
    
    if (index == 240)
    {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

#pragma mark UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    LoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:(loginViewController)
                                         animated:YES];
}



@end
