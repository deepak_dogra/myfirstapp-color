//
//  WebServiceHandler.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "WebServiceHandler.h"

@interface WebServiceHandler ()

@end

@implementation WebServiceHandler

-(void) connectWithServer :(NSString *)appendUrlString
        dataForPostRequest:(NSData *)jsonRequest
             typeOfRequest:(TypeOfRequest) typeOfRequest
     withCompletionHandler:(void (^)(NSMutableArray *))callBackBlock ;
{
    NSString *urlString = [[NSString alloc]initWithFormat:@"http://127.0.0.1:5000/todo/api/v1.0/%@",appendUrlString];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if(jsonRequest)
    {
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonRequest];
    }
    else
    {
        [request setHTTPMethod:@"GET"];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    self.tempStorageBlock  = callBackBlock;
    self.requestIdentifier = typeOfRequest;
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

#pragma mark NSURLConnectionDataDelegate Methods.

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData :data];
}

@end
