//
//  PageContentViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 21/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "PageContentViewController.h"
#import "ColorCollectionModel.h"
#import "ColorOperations.h"
#import "CustomColorCellTableViewCell.h"
#import "ColorCombinationViewController.h"
#import "LoginViewController.h"


@interface PageContentViewController ()<UITableViewDataSource,UITableViewDelegate>

/*!
 @brief The PageContentViewController class's tableOutlet.
 @discussion tableOutlet is used for adjusting the height of rows based on no of colors we are going to display in the page.
 */
@property (strong, nonatomic) IBOutlet UITableView *tableOutlet;

/*!
 @brief The PageContentViewController class's colorArray.
 @discussion We will store the response that we will get from the server in this property.It will contain  custom ColorCollectionModel objects.
 */
@property NSMutableArray *colorArray;

@end

@implementation PageContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor   = [UIColor clearColor];
    self.tableView.scrollEnabled    = NO;

    [self.tableView registerNib:[UINib nibWithNibName:@"CustomColorCellTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:@"Cell"];
    
    NSString *appendString;
    appendString = [[NSString alloc]initWithFormat:@"color/page_num=%lu",(self.pageIndex)+1];
    
    //making an api call from here for page_number.
    
    ColorOperations *colorOperationsObj = [[ColorOperations alloc]init];
    
    [colorOperationsObj connectWithServer:appendString
                       dataForPostRequest:nil
                            typeOfRequest:3
                    withCompletionHandler:^(NSMutableArray *colorListArray)
     {
         self.colorArray = colorListArray;
         ColorCollectionModel *colorObj = (ColorCollectionModel*) [self.colorArray objectAtIndex:0];
         
         NSString *colorFamily = colorObj.colorFamily;
         NSNumber *pageNumber  = colorObj.pageNo;
         UIColor *color=[UIColor colorWithRed:[colorObj.rgbRedValue floatValue]/255.0f
                                        green:[colorObj.rgbGreenValue floatValue]/255.0f
                                         blue:[colorObj.rgbBlueValue floatValue ]/255.0f
                                        alpha:1.0f];
         // setting the colorFamilyBlock .
         
         if(self.getColorFamilyBlock)
         {
             self.getColorFamilyBlock(colorFamily,color,pageNumber);
         }
         [self.tableView reloadData];
     }];
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.colorArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"Cell";
    
    CustomColorCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    ColorCollectionModel *colorObj= (ColorCollectionModel*) [self.colorArray objectAtIndex:indexPath.row];
    
    if (cell == nil)
    {
        cell = [[CustomColorCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell.contentView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.contentView.layer setBorderWidth:2.0f];

    cell.shadeNameLabel.text = colorObj.shadeName;
    cell.bottomConstraint.constant  = -10.0f;
    cell.colorFamilyLabel.textColor = [UIColor clearColor];
    
    UIColor *backGroundColor=[UIColor colorWithRed:[colorObj.rgbRedValue floatValue]/255.0f
                                             green:[colorObj.rgbGreenValue floatValue]/255.0f
                                              blue:[colorObj.rgbBlueValue floatValue ]/255.0f
                                             alpha:1.0f];
    
    const CGFloat *componentColors = CGColorGetComponents(backGroundColor.CGColor);
    
    UIColor *newColor = [[UIColor alloc] initWithRed:(1.0 - componentColors[0])
                                               green:(1.0 - componentColors[1])
                                                blue:(1.0 - componentColors[2])
                                               alpha:componentColors[3]];
    cell.backgroundColor = backGroundColor;
    cell.shadeNameLabel.textColor = newColor;
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(self.colorArray.count==4)   //display four colors.
    {
        return self.tableOutlet.frame.size.height/4;
    }
    else   //display 8 colors.
    {
        return self.tableOutlet.frame.size.height/8;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ColorCollectionModel *colorObj = (ColorCollectionModel*) [self.colorArray objectAtIndex:indexPath.row];
    
    ColorCombinationViewController *colorCombinationViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"ColorCombinationViewController"];
    [self.navigationController pushViewController:(colorCombinationViewController)
                                         animated:YES];
    //we will find different combination of colors for the selected palette.
    //Here we are setting the property of colorCombinationViewController where we will use this selected palette.
    colorCombinationViewController.colorPaletteObj=colorObj;
}

@end
