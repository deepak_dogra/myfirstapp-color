//
//  CityListCollectionModel.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 14/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityListCollectionModel : NSObject

@property NSString *cityName;
@property NSString *stateName;
@property NSNumber *cityId;

@end
