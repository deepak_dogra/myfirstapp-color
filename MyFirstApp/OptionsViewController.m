//
//  OptionsViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 09/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "OptionsViewController.h"
#import "SearchViewController.h"
#import "ViewPaletteViewController.h"
#import "ImagePickerViewController.h"
#import "UserDataCollectionModel.h"
#import "LoginViewController.h"
#import "SavedPaletteListViewController.h"

@interface OptionsViewController ()<UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *shortListButton;

/*!
 @discussion When user tap on this button then it will show the list of saved Palettes if the user is login otherwise it will redirect the user to login page.
 */
- (IBAction)shortListButtonTapped:(id)sender;
/*!
 @discussion When user tap on this button then it Search the colors either by shadename or shadecode.
 */
- (IBAction)searchButtonTapped:(id)sender;
/*!
 @discussion When user tap on this button then it will display the list of Palettes.
 */
- (IBAction)viewPaletteButtonTapped:(id)sender;
/*!
 @discussion When user tap on this button then it accesses the user's camera and gallery.
 */
- (IBAction)clickPhotoButtonTapped:(id)sender;

@end

@implementation OptionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem=self.shortListButton;
}

#pragma mark Show Saved palette Method

- (IBAction)shortListButtonTapped:(id)sender
{
    UserDataCollectionModel *userDataModelObj = [UserDataCollectionModel sharedInstance];
    
    //if user is logged in only then we will show the list of saved palettes.
    
    if([userDataModelObj.loginStatus isEqual:@"1"])
    {
        SavedPaletteListViewController *savedPaletteListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SavedPaletteListViewController"];
        [self.navigationController pushViewController:(savedPaletteListViewController)
                                             animated:YES];
        
    }
    else //show an alert to user and redirect the user to login page.(using delegate method of UIAlertViewDelegate)
    {
        [[[UIAlertView alloc] initWithTitle:@"Login Required!!"
                                    message:@"Login or Register First"
                                   delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Ok", nil] show];
    }
}

#pragma mark Custom Options Methods

- (IBAction)searchButtonTapped:(id)sender
{
    SearchViewController *searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [self.navigationController pushViewController:(searchViewController)
                                         animated:YES];
}

- (IBAction)viewPaletteButtonTapped:(id)sender
{
    ViewPaletteViewController *viewPaletteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPaletteViewController"];
    [self.navigationController pushViewController:(viewPaletteViewController)
                                         animated:YES];
}

- (IBAction)clickPhotoButtonTapped:(id)sender
{
    ImagePickerViewController *imagePickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImagePickerViewController"];
    [self.navigationController pushViewController:(imagePickerViewController)
                                         animated:YES];
}

#pragma mark UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    LoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:(loginViewController)
                                         animated:YES];
}

@end
