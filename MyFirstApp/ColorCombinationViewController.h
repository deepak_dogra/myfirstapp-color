//
//  ColorCombinationViewController.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 22/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorCollectionModel.h"


@interface ColorCombinationViewController : UITableViewController

/*!
 @brief The ColorCombinationViewController class's colorPaletteObj.
 @discussion Stores the (ColorCollectionModel *) for which we will find the analogous,monochromatic and complimentary colors.
 */
@property ColorCollectionModel *colorPaletteObj;

@end
