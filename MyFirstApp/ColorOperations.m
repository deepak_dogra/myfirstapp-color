//
//  ColorOperations.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 13/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "ColorOperations.h"
#import "ColorCollectionModel.h"

@implementation ColorOperations

#pragma mark NSURLConnectionDataDelegate Methods.

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *errorMessage=nil;
    NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData: self.responseData
                                                                     options: NSJSONReadingMutableContainers
                                                                       error: &errorMessage];
    NSArray *tempArray=[resultDictionary objectForKey:@"Colors"];
    NSMutableArray *colorArray=[[NSMutableArray alloc]init];
    
    for (NSDictionary *dict in tempArray) //storing the response in colorArray.
    {
        ColorCollectionModel *colorModelObject = [[ColorCollectionModel alloc]init];
        
        colorModelObject.rgbBlueValue  = [dict valueForKey:@"blue"];
        colorModelObject.rgbGreenValue = [dict valueForKey:@"green"];
        colorModelObject.rgbRedValue   = [dict valueForKey:@"red"];
        colorModelObject.pageNo        = [dict valueForKey:@"pageNo"];
        colorModelObject.colorFamily   = [dict valueForKey:@"colourFamily"];
        colorModelObject.shadePosition = [dict valueForKey:@"shadePosition"];
        colorModelObject.shadeCode     = [dict valueForKey:@"shadeCode"];
        colorModelObject.shadeName     = [dict valueForKey:@"shadeName"];
        
        [colorArray addObject:colorModelObject];
    }
    
    if(self.tempStorageBlock)
    {
        self.tempStorageBlock(colorArray);
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"failed");
}



@end
