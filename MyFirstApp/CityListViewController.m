
//
//  CityListViewController.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 07/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "CityListViewController.h"
#import "RegisterViewController.h"
#import "UserLoginAndRegistration.h"
#import "CityListCollectionModel.h"

@interface CityListViewController ()<UITableViewDataSource,UITableViewDelegate>

/*!
 @brief The CityListViewController class's listOfCities.
 @discussion Stores the list of available cities that we get as a response from server.
 */
@property (nonatomic) NSMutableArray *listOfCities;

@end

@implementation CityListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *appendString;
    appendString = [[NSString alloc]initWithFormat:@"city"];
    UserLoginAndRegistration *userObj = [[UserLoginAndRegistration alloc]init];
    
    //making an api call from here for fetching the List of Cities.
    
    [userObj connectWithServer:appendString
            dataForPostRequest:nil
                 typeOfRequest:2
         withCompletionHandler:^(NSMutableArray *cityList)
            {
                self.listOfCities = [[NSMutableArray alloc]init];
                self.listOfCities = cityList;
                [self.tableView reloadData];
            }];
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listOfCities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    CityListCollectionModel *cityListObj = (CityListCollectionModel*) [self.listOfCities objectAtIndex:indexPath.row];
    cell.textLabel.text=cityListObj.cityName;
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = selectedCell.textLabel.text;
    
    //user taps on a row and select a city here.
    
    if(self.getCityNameBlock)
    {
        self.getCityNameBlock(cellText);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
