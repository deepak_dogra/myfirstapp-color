//
//  CustomPaletteCellTableViewCell.h
//  MyFirstApp
//
//  Created by Deepak Dogra on 25/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPaletteCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@end
