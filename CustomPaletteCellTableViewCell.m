//
//  CustomPaletteCellTableViewCell.m
//  MyFirstApp
//
//  Created by Deepak Dogra on 25/09/15.
//  Copyright (c) 2015 Deepak Dogra. All rights reserved.
//

#import "CustomPaletteCellTableViewCell.h"

@implementation CustomPaletteCellTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    UIColor *color1 = self.label1.backgroundColor;
    UIColor *color2 = self.label2.backgroundColor;
    UIColor *color3 = self.label3.backgroundColor;

    [super setSelected:selected animated:animated];
    
    if (selected)
    {
        self.label1.backgroundColor = color1;
        self.label2.backgroundColor = color2;
        self.label3.backgroundColor = color3;
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    UIColor *color1 = self.label1.backgroundColor;
    UIColor *color2 = self.label2.backgroundColor;
    UIColor *color3 = self.label3.backgroundColor;
    
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted)
    {
        self.label1.backgroundColor = color1;
        self.label2.backgroundColor = color2;
        self.label3.backgroundColor = color3;
    }
}

@end
